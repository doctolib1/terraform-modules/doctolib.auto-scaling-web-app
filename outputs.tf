output "aws_asg_id" {
  description = "The AWS auto scaling group id"
  value       = aws_autoscaling_group.webapp_asg.id
}

output "aws_asg_name" {
  description = "The AWS auto scaling group name"
  value       = aws_autoscaling_group.webapp_asg.name
}

output "aws_key_pair" {
  description = "The AWS key pair created"
  value       = aws_key_pair.key_pair
}

output "aws_launch_template_id" {
  description = "The AWS launch template id"
  value       = aws_launch_template.instance_template.id
}

output "aws_placement_group_id" {
  description = "The AWS placement group id"
  value       = aws_placement_group.webapp.id
}