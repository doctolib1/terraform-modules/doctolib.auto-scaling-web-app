# Common configurations
variable "app_name" {
  description = "The application name (must be unique)."
  type        = string
}

variable "tags" {
  description = "A map of string containing key value pair to tag created resources."
  type        = map(string)
  default     = {}
}

# Placement startegy
variable "asg_placement_name" {
  description = "The name of the placement strategy group"
  type        = string
}

variable "asg_placement_strategy" {
  description = "The placement strategy"
  type        = string
  default     = "partition"

  validation {
    condition     = contains(["cluster", "partition", "spread"], var.asg_placement_strategy)
    error_message = "Can be \"cluster\", \"partition\" or \"spread\"."
  }
}

# ASG configuration
variable "asg_availability_zones" {
  description = "A list of one or more availability zones for the group."
  type        = list(string)
}
variable "asg_max_size" {
  description = "The maximum size of the Auto Scaling Group."
  type        = number
  default     = 3
}
variable "asg_min_size" {
  description = "The minimum size of the Auto Scaling Group."
  type        = number
  default     = 1
}
variable "asg_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group."
  type        = number
  default     = 1
}

# EC2 instance configuration
variable "asg_instance_image_id" {
  description = "The AMI from which to launch the instance."
  type        = string
  default     = "ami-0f7cd40eac2214b37"
}
variable "asg_instance_type" {
  description = "The type of the instance."
  type        = string
  default     = "t2.micro"
}

variable "asg_instance_user_data" {
  description = "The init script launch by instances."
  type        = string
  default     = ""
}

variable "aws_alb_target_group_arn" {
  description = "The alb target group arn."
  type        = string
}

# Security configurations
variable "asg_authorized_ips" {
  description = "The list of authorized IPs."
  type        = list(string)
  default     = []
}

# IAM configurations
variable "aws_iam_instance_profile" {
  description = "The IAM instance profile."
  type        = string
}
