resource "aws_placement_group" "webapp" {
  name     = var.asg_placement_name     # doctolib
  strategy = var.asg_placement_strategy # partition
  tags     = var.tags
}

resource "aws_autoscaling_group" "webapp_asg" {
  # General configurations
  name                      = var.app_name
  force_delete              = true
  placement_group           = aws_placement_group.webapp.id
  enabled_metrics           = []

  # Instances configurations
  launch_template {
    id      = aws_launch_template.instance_template.id
    version = aws_launch_template.instance_template.latest_version
  }
#  warm_pool {
#    pool_state                  = "Stopped"
#    min_size                    = 1
#    max_group_prepared_capacity = 10
#  }

  # Metadata configurations
  dynamic "tag" {
    for_each = var.tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  # Scaling configurations
  max_size         = var.asg_max_size
  min_size         = var.asg_min_size
  desired_capacity = var.asg_desired_capacity
  timeouts {
    delete = "15m"
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes = [tags]
  }
  #  termination_policies = "OldestInstance"
  suspended_processes       = []
  termination_policies      = ["OldestLaunchTemplate"]
  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
#    triggers = ["tag"]
  }

  # Load balancing configurations
  load_balancers            = []
  health_check_type         = "ELB"
  health_check_grace_period = 300

  # Network configurations
  availability_zones = var.asg_availability_zones
  target_group_arns  = [var.aws_alb_target_group_arn]
#  vpc_zone_identifier = [data.aws_vpc.default.id]
  # Notifications configurations
  #  initial_lifecycle_hook {
  #    name                 = "foobar"
  #    default_result       = "CONTINUE"
  #    heartbeat_timeout    = 2000
  #    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
  #
  #    notification_metadata = <<EOF
  #{
  #  "foo": "bar"
  #}
  #EOF
  #
  #    notification_target_arn = "arn:aws:sqs:us-east-1:444455556666:queue1*"
  #    role_arn                = "arn:aws:iam::123456789012:role/S3Access"
  #  }
}