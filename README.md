# doctolib.auto-scaling-web-app

This Terraform module aims to provide an easy way to deploy an ASG for EC2 instances on AWS.
This module provision : 

- An ssh key pair
- An AWS launch template
- Security groups to allow access using ICMP, HTTP(s) and SSH to a restricted list of IPs.
- And AWS auto scaling group.

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.15 |
| aws | >= 3.39.0 |
| tls | >= 3.1.0 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.39.0 |
| tls | >= 3.1.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| app\_name | The application name (must be unique). | `string` | n/a | yes |
| asg\_authorized\_ips | The list of authorized IPs. | `list(string)` | `[]` | no |
| asg\_availability\_zones | A list of one or more availability zones for the group. | `list(string)` | n/a | yes |
| asg\_desired\_capacity | The number of Amazon EC2 instances that should be running in the group. | `number` | `1` | no |
| asg\_instance\_image\_id | The AMI from which to launch the instance. | `string` | `"ami-0f7cd40eac2214b37"` | no |
| asg\_instance\_type | The type of the instance. | `string` | `"t2.micro"` | no |
| asg\_instance\_user\_data | The init script launch by instances. | `string` | `""` | no |
| asg\_max\_size | The maximum size of the Auto Scaling Group. | `number` | `3` | no |
| asg\_min\_size | The minimum size of the Auto Scaling Group. | `number` | `1` | no |
| asg\_placement\_name | The name of the placement strategy group | `string` | n/a | yes |
| asg\_placement\_strategy | The placement strategy | `string` | `"partition"` | no |
| aws\_alb\_target\_group\_arn | The alb target group arn. | `string` | n/a | yes |
| aws\_iam\_instance\_profile | The IAM instance profile. | `string` | n/a | yes |
| tags | A map of string containing key value pair to tag created resources. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_asg\_id | The AWS auto scaling group id |
| aws\_asg\_name | The AWS auto scaling group name |
| aws\_key\_pair | The AWS key pair created |
| aws\_launch\_template\_id | The AWS launch template id |
| aws\_placement\_group\_id | The AWS placement group id |