resource "aws_launch_template" "instance_template" {
  name_prefix   = var.app_name
  image_id      = var.asg_instance_image_id
  instance_type = var.asg_instance_type
  user_data     = var.asg_instance_user_data

  key_name   = aws_key_pair.key_pair.key_name

  # Security configurations
  vpc_security_group_ids = [
    module.ec2_sg.security_group_id,
    module.dev_ssh_sg.security_group_id,
    aws_default_security_group.default.id
  ]

  # IAM configurations
  iam_instance_profile {
    name = var.aws_iam_instance_profile
  }

  tags = var.tags
}